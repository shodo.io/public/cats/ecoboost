package fr.ca.cats.ecoboost.infra.saver;

import fr.ca.cats.ecoboost.domain.saving.SaverRepository;
import fr.ca.cats.ecoboost.domain.saving.model.Saver;
import fr.ca.cats.ecoboost.domain.saving.model.SaverId;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class InMemorySaverRepository implements SaverRepository {
    private final Map<SaverId, Saver> map = new HashMap<>();

    @Override
    public Saver register(Saver saver) {
        map.put(saver.id(), saver);
        return saver;
    }

    @Override
    public Saver retrieve(SaverId saverId) {
        return Optional.ofNullable(map.get(saverId)).orElseThrow();
    }
}
