package fr.ca.cats.ecoboost.infra.saver;

import fr.ca.cats.ecoboost.domain.saving.AccountRepository;
import fr.ca.cats.ecoboost.domain.saving.model.SaverId;
import fr.ca.cats.ecoboost.domain.saving.model.SavingAccount;

import java.util.HashMap;

public class InMemoryAccountRepository implements AccountRepository {
    private static final HashMap<SaverId, SavingAccount> accounts = new HashMap<>();

    @Override
    public SavingAccount create(SaverId id) {
        SavingAccount savingAccount = SavingAccount.create();
        accounts.put(id, savingAccount);
        return savingAccount;
    }

    @Override
    public SavingAccount find(SaverId saverId) {
        return accounts.get(saverId);
    }
}
