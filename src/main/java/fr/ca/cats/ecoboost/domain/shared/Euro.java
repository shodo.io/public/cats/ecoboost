package fr.ca.cats.ecoboost.domain.shared;

import java.math.BigDecimal;
import java.math.RoundingMode;

public record Euro(BigDecimal value) {
    public static Euro ZERO = Euro.of(BigDecimal.ZERO);

    public Euro {
        if (value.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException("Amount cannot be negative");
        }
    }

    public static Euro of(BigDecimal value) {
        return new Euro(value.setScale(2, RoundingMode.HALF_UP));
    }

    @Override
    public String toString() {
        return value.toPlainString() + " €";
    }

    public Percentage rateOf(Euro amount) {
        return Percentage.of(value.divide(amount.value, 4, RoundingMode.HALF_UP));
    }

    public Euro divide(int divisor) {
        return Euro.of(value.divide(BigDecimal.valueOf(divisor), 4, RoundingMode.HALF_UP));
    }

    public Euro subtract(Euro subtrahend) {
        return Euro.of(value.subtract(subtrahend.value));
    }

    public boolean isPositive() {
        return value.compareTo(BigDecimal.ZERO) > 0;
    }

    public Euro add(Euro augend) {
        return Euro.of(value.add(augend.value));
    }

    public boolean isGreaterOrEqualsOf(Euro euro) {
        return value.compareTo(euro.value)>=0;
    }
}
