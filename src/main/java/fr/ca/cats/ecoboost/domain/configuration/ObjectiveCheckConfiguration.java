package fr.ca.cats.ecoboost.domain.configuration;

public record ObjectiveCheckConfiguration(
        ObjectiveCheckType check
) {
}
