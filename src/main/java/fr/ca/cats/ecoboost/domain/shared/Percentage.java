package fr.ca.cats.ecoboost.domain.shared;

import java.math.BigDecimal;


public record Percentage(BigDecimal rate) {

    public static Percentage FIFTY = Percentage.of(BigDecimal.valueOf(0.5));

    public static Percentage of(BigDecimal rate) {
        return new Percentage(rate.multiply(BigDecimal.valueOf(100)));
    }

    @Override
    public String toString() {
        return rate + "%";
    }

    public boolean lessThan(Percentage compared) {
        return rate.compareTo(compared.rate) < 0;
    }
}

