package fr.ca.cats.ecoboost.domain.saving.model;

import java.util.UUID;

public record SaverId(UUID value) {
    public static SaverId generate() {
        return new SaverId(UUID.randomUUID());
    }
}
