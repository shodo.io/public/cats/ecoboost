package fr.ca.cats.ecoboost.domain.objectives.model;

public enum Periodicity {
    MONTHLY,
    YEARLY
}
