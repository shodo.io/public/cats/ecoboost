package fr.ca.cats.ecoboost.domain.usecase.saving;

import fr.ca.cats.ecoboost.domain.objectives.model.Objective;
import fr.ca.cats.ecoboost.domain.saving.AccountRepository;
import fr.ca.cats.ecoboost.domain.saving.SaverRepository;
import fr.ca.cats.ecoboost.domain.saving.model.Profile;
import fr.ca.cats.ecoboost.domain.saving.model.Saver;
import io.vavr.control.Either;

public class DefineObjective {

    private final SaverRepository saverRepository;
    private final AccountRepository accountRepository;

    public DefineObjective(SaverRepository saverRepository, AccountRepository accountRepository) {
        this.saverRepository = saverRepository;
        this.accountRepository = accountRepository;
    }

    public Either<UnReachableObjectiveError, Saver> act(Profile profile, Objective objective) {
        if (objective.isReachable(profile.disposableIncome())) {
            Saver saver = saverRepository.register(Saver.create(profile, objective));
            accountRepository.create(saver.id());
            return Either.right(saver);
        }
        return Either.left(new UnReachableObjectiveError(objective.savingRates(profile.disposableIncome())));
    }
}
