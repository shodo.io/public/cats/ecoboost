package fr.ca.cats.ecoboost.domain.saving.model;

import fr.ca.cats.ecoboost.domain.objectives.model.Objective;

public record Saver(
        SaverId id,
        Profile profile,
        Objective objective
) {

    public static Saver create(Profile profile, Objective objective) {
        return new Saver(SaverId.generate(), profile, objective);
    }
}
