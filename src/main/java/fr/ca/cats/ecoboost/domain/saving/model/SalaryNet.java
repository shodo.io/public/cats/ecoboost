package fr.ca.cats.ecoboost.domain.saving.model;

import fr.ca.cats.ecoboost.domain.objectives.model.Periodicity;
import fr.ca.cats.ecoboost.domain.shared.Euro;

public record SalaryNet(Euro value, Periodicity periodicity) {
}
