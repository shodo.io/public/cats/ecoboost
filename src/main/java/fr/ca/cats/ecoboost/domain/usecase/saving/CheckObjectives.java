package fr.ca.cats.ecoboost.domain.usecase.saving;

import fr.ca.cats.ecoboost.domain.configuration.ObjectiveCheckConfiguration;
import fr.ca.cats.ecoboost.domain.configuration.ObjectiveCheckType;
import fr.ca.cats.ecoboost.domain.saving.SaverRepository;
import fr.ca.cats.ecoboost.domain.saving.model.Saver;
import fr.ca.cats.ecoboost.domain.saving.model.SaverId;
import fr.ca.cats.ecoboost.domain.saving.model.SavingAccount;
import fr.ca.cats.ecoboost.domain.shared.Clock;
import fr.ca.cats.ecoboost.domain.shared.Euro;

public class CheckObjectives {
    private final Clock clock;
    private final ObjectiveCheckConfiguration configuration;
    private final SaverRepository saverRepository;

    public CheckObjectives(Clock clock, ObjectiveCheckConfiguration configuration, SaverRepository saverRepository) {
        this.clock = clock;
        this.configuration = configuration;
        this.saverRepository = saverRepository;
    }

    public boolean invoke(SaverId saverId, SavingAccount savingAccount) {
        if (configuration.check() == ObjectiveCheckType.END_OF_MONTH) {
            if (clock.isLastDayOfMonth()) {
                Euro currentMonthBalance = savingAccount.balanceOf(clock.now().getYear(), clock.now().getMonth());
                Saver saver = saverRepository.retrieve(saverId);
                return currentMonthBalance.isGreaterOrEqualsOf(saver.objective().installment().monthlyAmount());
            }
        }
        throw new IllegalStateException("YOU SHOULD DEFINE OBJECTIVE CHECK CONFIGURATION");
    }
}
