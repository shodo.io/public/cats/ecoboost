package fr.ca.cats.ecoboost.domain.usecase.saving;

import fr.ca.cats.ecoboost.domain.shared.Percentage;

public record UnReachableObjectiveError(Percentage savingsRate) {
}
