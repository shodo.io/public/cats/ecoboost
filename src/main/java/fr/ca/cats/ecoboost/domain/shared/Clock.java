package fr.ca.cats.ecoboost.domain.shared;

import java.time.LocalDate;
import java.time.LocalDateTime;

public interface Clock {

    default LocalDateTime now(){
        return LocalDateTime.now();
    }

    default boolean isLastDayOfMonth() {
        LocalDate localDate = now().toLocalDate();
        LocalDate lastDayOfMonth = localDate.withDayOfMonth(localDate.lengthOfMonth());
        return localDate.equals(lastDayOfMonth);
    }
}
