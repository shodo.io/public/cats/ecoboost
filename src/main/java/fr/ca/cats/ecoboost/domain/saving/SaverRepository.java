package fr.ca.cats.ecoboost.domain.saving;

import fr.ca.cats.ecoboost.domain.saving.model.Saver;
import fr.ca.cats.ecoboost.domain.saving.model.SaverId;

public interface SaverRepository {

    Saver register(Saver saver);
    Saver retrieve(SaverId saverId);
}
