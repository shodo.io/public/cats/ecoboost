package fr.ca.cats.ecoboost.domain.saving.model;

import java.util.UUID;

public record SavingAccountId(UUID value) {
    public static SavingAccountId generate() {
        return new SavingAccountId(UUID.randomUUID());
    }
}
