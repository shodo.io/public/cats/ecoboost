package fr.ca.cats.ecoboost.domain.saving;

import fr.ca.cats.ecoboost.domain.saving.model.SaverId;
import fr.ca.cats.ecoboost.domain.saving.model.SavingAccount;

public interface AccountRepository {
    SavingAccount create(SaverId id);

    SavingAccount find(SaverId saverId);

}
