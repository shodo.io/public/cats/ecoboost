package fr.ca.cats.ecoboost.domain.usecase.saving;

import fr.ca.cats.ecoboost.domain.saving.AccountRepository;
import fr.ca.cats.ecoboost.domain.saving.model.SaverId;
import fr.ca.cats.ecoboost.domain.saving.model.SavingAccount;

import java.time.LocalDateTime;

public class Save {
    private final AccountRepository accountRepository;

    public Save(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public void invoke(SaverId saverId, SavingAmount savingAmount, LocalDateTime savingDate) {
        SavingAccount savingAccount = accountRepository.find(saverId);
        savingAccount.deposit(savingAmount, savingDate);
    }
}
