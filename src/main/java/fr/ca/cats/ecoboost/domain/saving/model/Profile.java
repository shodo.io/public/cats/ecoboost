package fr.ca.cats.ecoboost.domain.saving.model;

import fr.ca.cats.ecoboost.domain.shared.Euro;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public record Profile(
        String pseudo,
        LocalDate birthDate,
        SalaryNet salaryNet,
        EssentialCharges charges
) {

    public int age() {
        return (int) ChronoUnit.YEARS.between(birthDate, LocalDate.now());
    }

    public Euro disposableIncome(){
        return salaryNet.value().subtract(charges.value());
    }
}
