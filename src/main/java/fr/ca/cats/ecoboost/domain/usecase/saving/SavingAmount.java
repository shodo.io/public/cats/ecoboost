package fr.ca.cats.ecoboost.domain.usecase.saving;

import fr.ca.cats.ecoboost.domain.shared.Euro;

public record SavingAmount(Euro value) {

    public SavingAmount {
        if (!value.isPositive()){
            throw new IllegalArgumentException("Saving value cannot be null");
        }
    }
}
