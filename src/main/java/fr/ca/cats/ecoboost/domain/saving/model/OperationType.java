package fr.ca.cats.ecoboost.domain.saving.model;

public enum OperationType {
    DEPOSIT, WITHDRAWAL
}
