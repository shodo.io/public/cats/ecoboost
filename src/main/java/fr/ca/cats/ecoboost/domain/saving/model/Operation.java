package fr.ca.cats.ecoboost.domain.saving.model;

import fr.ca.cats.ecoboost.domain.usecase.saving.SavingAmount;

import java.time.LocalDateTime;

public record Operation(SavingAmount amount, LocalDateTime dateTime, OperationType type) {
}
