package fr.ca.cats.ecoboost.domain.objectives.model;

import fr.ca.cats.ecoboost.domain.shared.Euro;
import fr.ca.cats.ecoboost.domain.shared.Percentage;

import static fr.ca.cats.ecoboost.domain.shared.Percentage.FIFTY;

public record Objective(Euro globalAmount,
                        Installment installment) {

    public boolean isReachable(Euro disposableIncome) {
        return savingRates(disposableIncome).lessThan(FIFTY);
    }

    public Percentage savingRates(Euro disposableIncome) {
        return rateOf(disposableIncome);
    }

    private Percentage rateOf(Euro amount){
        return installment().monthlyAmount().rateOf(amount);
    }

    public record Installment(Euro amount, Periodicity periodicity) {
        public Euro monthlyAmount() {
            return switch (periodicity) {
                case MONTHLY -> amount;
                case YEARLY -> amount.divide(12);
            };
        }
    }
}
