package fr.ca.cats.ecoboost.domain.usecase.saving;

import fr.ca.cats.ecoboost.domain.saving.SaverRepository;
import fr.ca.cats.ecoboost.domain.saving.model.Saver;
import fr.ca.cats.ecoboost.domain.saving.model.SaverId;

public class RetrieveSaver {

    SaverRepository saverRepository;

    public RetrieveSaver(SaverRepository saverRepository) {
        this.saverRepository = saverRepository;
    }

    public Saver act(SaverId saverId) {
        return saverRepository.retrieve(saverId);
    }
}
