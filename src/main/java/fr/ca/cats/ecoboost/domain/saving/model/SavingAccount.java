package fr.ca.cats.ecoboost.domain.saving.model;

import fr.ca.cats.ecoboost.domain.shared.Euro;
import fr.ca.cats.ecoboost.domain.usecase.saving.SavingAmount;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

public record SavingAccount(SavingAccountId id, List<Operation> operations) {
    public static SavingAccount create() {
        return new SavingAccount(SavingAccountId.generate(), new ArrayList<>());
    }

    public Euro balance() {
        return balance(operations);
    }

    private Euro balance(List<Operation> operationsList) {
        if (operationsList.isEmpty()) {
            return Euro.of(BigDecimal.ZERO);
        } else {
            return operationsList.stream().map(o -> o.amount().value()).reduce(Euro.ZERO, Euro::add);
        }
    }

    public void deposit(SavingAmount savingAmount, LocalDateTime operationDate) {
        Operation operation = new Operation(savingAmount, operationDate, OperationType.DEPOSIT);
        operations.add(operation);
    }

    public Euro balanceOf(int year, Month month) {
        return balance(
                operations.stream().filter(o -> o.dateTime().getYear() == year && o.dateTime().getMonth() == month).toList()
        );
    }
}
