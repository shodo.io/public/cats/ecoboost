Feature: Objectif mensuel

  Rule: prise en compte de l'epargne sur tout le mois
    Scenario: Objectif atteint
      Given a standard saver
      And objective check is configured at end of month
      And He saved 500.00€ the 2024-05-30
      And we are the 2024-05-31
      When objectives are checked
      Then his objective should be reached