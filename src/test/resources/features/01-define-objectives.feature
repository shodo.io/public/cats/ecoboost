Feature: Define Objective

  Rule: should register user as a saver
    Scenario: Simple objective
      Given a standard user
      When he defines 5000.00€ at a rate of 400.00€ per month as objective
      Then he should be registered as saver with 5000.00€ at a rate of 400.00€ per month as objective

    Scenario: Impossible objective
      Given a standard user
      When he defines 5000.00€ at a rate of 500.00€ per month as objective
      Then his savings rate should be 50.00%
      And he should be alerted that objective is unreachable
      And he should not be registered

  Rule: should create an account
    Scenario: Simple objective
      Given a standard user
        When he defines 5000.00€ at a rate of 400.00€ per month as objective
        Then an account should be created