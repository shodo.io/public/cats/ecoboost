Feature: User definitions

  Scenario: Standard User
    Given a standard user
    * He is known as "Jimmy"
    * He is 27 years old
    * He earns 2000.00€ per month
    * His essential charges are 1000.00€ per month
