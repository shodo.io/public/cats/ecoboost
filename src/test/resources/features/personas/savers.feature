Feature: Saver definitions

  Scenario: Standard Saver
    Given a standard saver
    * He is known as "Jimmy"
    * He is 27 years old
    * He earns 2000.00€ per month
    * He wanted to save 5000.00€ at a rate of 400.00€ per month
    * His essential charges are 1000.00€ per month
