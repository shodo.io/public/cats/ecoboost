package fr.ca.cats.ecoboost.domain.objectives.model;

import fr.ca.cats.ecoboost.domain.shared.Euro;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static fr.ca.cats.ecoboost.domain.objectives.model.Periodicity.MONTHLY;
import static fr.ca.cats.ecoboost.domain.objectives.model.Periodicity.YEARLY;
import static org.assertj.core.api.Assertions.assertThat;

class ObjectiveTest {

    @Test
    void should_be_reachable_when_less_than_50_percent_of_disposable_income() {
        Objective objective = new Objective(euro(20000), new Objective.Installment(euro(499), MONTHLY));
        assertThat(objective.isReachable(euro(1000))).isTrue();
    }

    @Test
    void should_not_be_reachable_when_more_than_50_percent_of_disposable_income() {
        Objective objective = new Objective(euro(20000), new Objective.Installment(euro(500), MONTHLY));
        assertThat(objective.isReachable(euro(1000))).isFalse();
    }

    @Test
    void monthly_amount_should_be_installment_amount_on_a_monthly_rate() {
        Objective.Installment installment = new Objective.Installment(euro(400), MONTHLY);
        assertThat(installment.monthlyAmount()).isEqualTo(euro(400));
    }

    @Test
    void yearly_amount_should_be_divided_by_12() {
        Objective.Installment installment = new Objective.Installment(euro(1200), YEARLY);

        assertThat(installment.monthlyAmount()).isEqualTo(euro(100));
    }

    private Euro euro(int val) {
        return Euro.of(BigDecimal.valueOf(val));
    }
}