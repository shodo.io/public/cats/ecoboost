package fr.ca.cats.ecoboost.domain.saving.model;

import fr.ca.cats.ecoboost.domain.objectives.model.Periodicity;
import fr.ca.cats.ecoboost.domain.shared.Euro;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

class ProfileTest {

    @Test
    void age() {
        assertThat(profileWithAge(30).age()).isEqualTo(30);
        assertThat(profileWithAge(20).age()).isEqualTo(20);
    }

    private static Profile profileWithAge(int yearsToSubtract) {
        return new Profile(aPseudo(), LocalDate.now().minusYears(yearsToSubtract), aSalary(), someCharges());
    }

    private static EssentialCharges someCharges() {
        return new EssentialCharges(Euro.of(BigDecimal.valueOf(1000.00)), Periodicity.MONTHLY);
    }

    private static String aPseudo() {
        return "apseudo";
    }

    private static SalaryNet aSalary() {
        return new SalaryNet(Euro.of(BigDecimal.valueOf(2000.00)), Periodicity.MONTHLY);
    }
}