package fr.ca.cats.ecoboost.domain.usecase.saving;

import fr.ca.cats.ecoboost.domain.saving.AccountRepository;
import fr.ca.cats.ecoboost.domain.saving.SaverRepository;
import fr.ca.cats.ecoboost.domain.saving.model.Saver;
import fr.ca.cats.ecoboost.domain.saving.model.SavingAccount;
import fr.ca.cats.ecoboost.infra.saver.InMemoryAccountRepository;
import fr.ca.cats.ecoboost.infra.saver.InMemorySaverRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import util.Users;

import java.time.LocalDateTime;

import static fr.ca.cats.ecoboost.features.shared.ObjectiveParameter.objective;
import static util.Dsl.euro;


class SaveTest {

    private final SaverRepository saverRepository = new InMemorySaverRepository();
    private final AccountRepository accountRepository = new InMemoryAccountRepository();
    private final DefineObjective defineObjective = new DefineObjective(saverRepository, accountRepository);
    private final Save save = new Save(accountRepository);

    @Test
    void invoke() {
        Saver jimmy = jimmy();
        save.invoke(jimmy.id(), new SavingAmount(euro(100)), LocalDateTime.now());
        SavingAccount savingAccount = accountRepository.find(jimmy.id());
        Assertions.assertThat(savingAccount.balance()).isEqualTo(euro(100));
    }

    private Saver jimmy() {
        return defineObjective.act(Users.standardProfile(), objective("5000.00€ at a rate of 400.00€ per month")).get();
    }
}