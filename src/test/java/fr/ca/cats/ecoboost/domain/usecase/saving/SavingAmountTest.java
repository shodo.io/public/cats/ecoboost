package fr.ca.cats.ecoboost.domain.usecase.saving;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThatCode;
import static util.Dsl.euro;

class SavingAmountTest {

    @Test
    void should_not_be_able_to_save_negative_amount() {
        assertThatCode(() -> new SavingAmount(euro(-100))).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void should_not_be_able_to_save_no_amount() {
        assertThatCode(() -> new SavingAmount(euro(0))).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void should_be_able_to_save_amount() {
        assertThatCode(() -> new SavingAmount(euro(100))).doesNotThrowAnyException();
    }
}