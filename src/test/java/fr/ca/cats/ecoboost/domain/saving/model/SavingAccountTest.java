package fr.ca.cats.ecoboost.domain.saving.model;

import fr.ca.cats.ecoboost.domain.shared.Euro;
import fr.ca.cats.ecoboost.domain.usecase.saving.SavingAmount;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.Month;

import static org.assertj.core.api.Assertions.assertThat;
import static util.Dsl.euro;

class SavingAccountTest {

    @Test
    void balance_of_an_empty_account_should_be_0() {
        SavingAccount savingAccount = SavingAccount.create();
        assertThat(savingAccount.balance()).isEqualTo(euro(0));
    }

    @Test
    void deposit_should_be_added_to_balance() {
        SavingAccount savingAccount = SavingAccount.create();
        deposit(savingAccount, euro(100));
        assertThat(savingAccount.balance()).isEqualTo(euro(100));
    }

    @Test
    void multi_deposit_should_be_added_to_balance() {
        SavingAccount savingAccount = SavingAccount.create();
        deposit(savingAccount, euro(100), LocalDateTime.of(2023,5,10,10,0,0));
        deposit(savingAccount, euro(100), LocalDateTime.of(2024,5,10,10,0,0));
        deposit(savingAccount, euro(100), LocalDateTime.of(2024,5,13,10,0,0));
        assertThat(savingAccount.balance()).isEqualTo(euro(300));
    }

    @Test
    void shoudl_restrict_to_a_month() {
        SavingAccount savingAccount = SavingAccount.create();
        deposit(savingAccount, euro(100), LocalDateTime.of(2023,5,10,10,0,0));
        deposit(savingAccount, euro(100), LocalDateTime.of(2024,5,10,10,0,0));
        deposit(savingAccount, euro(100), LocalDateTime.of(2024,5,13,10,0,0));
        assertThat(savingAccount.balanceOf(2024, Month.MAY)).isEqualTo(euro(200));
    }


    private void deposit(SavingAccount savingAccount, Euro amount) {
        deposit(savingAccount, amount, LocalDateTime.now().minusMinutes(10));
    }

    private void deposit(SavingAccount savingAccount, Euro amount, LocalDateTime operationDate) {
        savingAccount.deposit(new SavingAmount(amount), operationDate);
    }
}