package fr.ca.cats.ecoboost.domain.usecase.saving;

import org.junit.jupiter.api.Test;
import util.LocalClock;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

class CheckObjectivesTest {

    @Test
    void lastDayOfMonth() {
        LocalClock localClock = new LocalClock();

        localClock.setNow(LocalDateTime.of(2024, 5, 31, 0, 0, 0));
        assertThat(localClock.isLastDayOfMonth()).isTrue();

        localClock.setNow(LocalDateTime.of(2024, 5, 15, 0, 0, 0));
        assertThat(localClock.isLastDayOfMonth()).isFalse();
    }
}