package fr.ca.cats.ecoboost.features.shared;

import fr.ca.cats.ecoboost.domain.objectives.model.Objective;
import fr.ca.cats.ecoboost.domain.objectives.model.Periodicity;
import fr.ca.cats.ecoboost.domain.saving.model.EssentialCharges;
import fr.ca.cats.ecoboost.domain.saving.model.SalaryNet;
import fr.ca.cats.ecoboost.domain.shared.Euro;
import io.cucumber.java.ParameterType;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ObjectiveParameter {
    public static final String DECIMAL_REGEX = "\\d+\\.\\d{2}";
    public static final String EURO_REGEX = DECIMAL_REGEX + "€";
    public static final String PERIODICITY_REGEX = "month|year";
    public static final String PERIODICAL_AMOUNT_REGEX = EURO_REGEX + " per " + PERIODICITY_REGEX;
    public static final String OBJECTIVE_REGEX = EURO_REGEX + " at a rate of " + PERIODICAL_AMOUNT_REGEX;

    @ParameterType(OBJECTIVE_REGEX)
    public static Objective objective(String objective) {
        Pattern pattern = Pattern.compile("(" + EURO_REGEX + ") at a rate of (" + PERIODICAL_AMOUNT_REGEX + ")");
        Matcher matcher = pattern.matcher(objective);
        if (matcher.find()) {
            Pair<Euro, Periodicity> parsed = parseAmountPeriodic(matcher.group(2));
            return new Objective(euro(matcher.group(1)), new Objective.Installment(parsed.first, parsed.second));
        } else {
            throw new IllegalArgumentException("unparsable " + objective);
        }
    }

    @ParameterType(PERIODICAL_AMOUNT_REGEX)
    public static SalaryNet salary(String objective) {
        Pair<Euro, Periodicity> parsed = parseAmountPeriodic(objective);
        return new SalaryNet(parsed.first, parsed.second);
    }

    @ParameterType(EURO_REGEX)
    public static Euro amount(String amount) {
        Pattern pattern = Pattern.compile("(" + EURO_REGEX + ")");
        Matcher matcher = pattern.matcher(amount);
        if (matcher.find()) {
            return euro(matcher.group(1));
        } else {
            throw new IllegalArgumentException("unparsable " + amount);
        }
    }

    @ParameterType(PERIODICAL_AMOUNT_REGEX)
    public static EssentialCharges charges(String charges) {
        Pair<Euro, Periodicity> parsed = parseAmountPeriodic(charges);
        return new EssentialCharges(parsed.first, parsed.second);
    }

    private static Pair<Euro, Periodicity> parseAmountPeriodic(String objective) {
        Pattern pattern = Pattern.compile("(" + EURO_REGEX + ") per (" + PERIODICITY_REGEX + ")");
        Matcher matcher = pattern.matcher(objective);
        if (matcher.find()) {
            return new Pair<>(euro(matcher.group(1)), periodicity(matcher.group(2)));
        } else {
            throw new IllegalArgumentException("unparsable " + objective);
        }
    }

    public static Periodicity periodicity(String periodicity) {
        return switch (periodicity) {
            case "month" -> Periodicity.MONTHLY;
            case "year" -> Periodicity.YEARLY;
            default -> null;
        };
    }

    public static Euro euro(String euro) {
        return Euro.of(new BigDecimal(euro.replaceAll("€", "")));
    }

    public record Pair<K, V>(K first, V second) {
    }
}
