package fr.ca.cats.ecoboost.features.saver;

import fr.ca.cats.ecoboost.domain.objectives.model.Objective;
import fr.ca.cats.ecoboost.domain.saving.model.SavingAccount;
import fr.ca.cats.ecoboost.domain.shared.Percentage;
import fr.ca.cats.ecoboost.domain.usecase.saving.DefineObjective;
import fr.ca.cats.ecoboost.domain.usecase.saving.UnReachableObjectiveError;
import fr.ca.cats.ecoboost.infra.saver.InMemoryAccountRepository;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import util.TestContext;

import static org.assertj.core.api.Assertions.assertThat;

public class ObjectiveDefinitions {

    private final DefineObjective defineObjective;
    private final TestContext testContext;
    private final InMemoryAccountRepository accountRepository;

    public ObjectiveDefinitions(DefineObjective defineObjective, TestContext testContext, InMemoryAccountRepository accountRepository) {
        this.defineObjective = defineObjective;
        this.testContext = testContext;
        this.accountRepository = accountRepository;
    }

    @When("he defines {objective} as objective")
    public void define(Objective objective) {
        defineObjective.act(testContext.currentProfile(), objective)
                .peek(testContext::setCurrentSaver)
                .peekLeft(testContext::setCurrentError);
    }

    @Then("he should be alerted that objective is unreachable")
    public void heShouldBeAlertedThatObjectiveIsUnreachable() {
        assertThat(testContext.currentError()).isInstanceOf(UnReachableObjectiveError.class);
    }

    @And("he should not be registered")
    public void heShouldNotBeRegistered() {
        assertThat(testContext.currentSaver()).isNull();
    }

    @Then("his savings rate should be {percentage}")
    public void hisSavingsRateShouldBe(Percentage percentage) {
        assertThat(testContext.currentError().savingsRate().rate()).isEqualByComparingTo(percentage.rate());
    }

    @Then("an account should be created")
    public void anAccountShouldBeCreated() {
        SavingAccount savingAccount = accountRepository.find(testContext.currentSaver());
        assertThat(savingAccount).isNotNull();
    }
}
