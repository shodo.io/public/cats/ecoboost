package fr.ca.cats.ecoboost.features.saver;

import fr.ca.cats.ecoboost.domain.configuration.ObjectiveCheckConfiguration;
import fr.ca.cats.ecoboost.domain.shared.Euro;
import fr.ca.cats.ecoboost.domain.usecase.saving.CheckObjectives;
import fr.ca.cats.ecoboost.domain.usecase.saving.Save;
import fr.ca.cats.ecoboost.domain.usecase.saving.SavingAmount;
import fr.ca.cats.ecoboost.infra.saver.InMemoryAccountRepository;
import fr.ca.cats.ecoboost.infra.saver.InMemorySaverRepository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;
import util.LocalClock;
import util.TestContext;

import java.time.LocalDateTime;

import static fr.ca.cats.ecoboost.domain.configuration.ObjectiveCheckType.END_OF_MONTH;

public class MonthlyObjectiveDefinitions {
    private boolean objectiveChecked;
    private final TestContext testContext;
    private final Save save;
    private final LocalClock clock;
    private final InMemoryAccountRepository accountRepository;
    private final InMemorySaverRepository saverRepository;

    public MonthlyObjectiveDefinitions(TestContext testContext, Save save, LocalClock clock, InMemoryAccountRepository accountRepository, InMemorySaverRepository saverRepository) {
        this.testContext = testContext;
        this.save = save;
        this.clock = clock;
        this.accountRepository = accountRepository;
        this.saverRepository = saverRepository;
    }

    @Given("objective check is configured at end of month")
    public void objectiveCheckIsConfiguredAtEndOfMonth() {
        testContext.setObjectiveConfiguration(new ObjectiveCheckConfiguration(END_OF_MONTH));
    }

    @Given("He saved {amount} the {date}")
    public void saved(Euro saving, LocalDateTime dateTime) {
        save.invoke(testContext.currentSaver(), new SavingAmount(saving), dateTime);
    }

    @Given("we are the {date}")
    public void weAreThe(LocalDateTime dateTime) {
        clock.setNow(dateTime);
    }

    @When("objectives are checked")
    public void objectivesAreChecked() {
        CheckObjectives checkObjectives = new CheckObjectives(clock, testContext.objectiveCheckConfiguration(), saverRepository);
        objectiveChecked = checkObjectives.invoke(testContext.currentSaver(), accountRepository.find(testContext.currentSaver()));
    }

    @Then("his objective should be reached")
    public void hisObjectiveShouldBeReached() {
        Assertions.assertThat(objectiveChecked).isTrue();
    }
}
