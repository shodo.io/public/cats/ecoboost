package fr.ca.cats.ecoboost.features.shared;

import fr.ca.cats.ecoboost.domain.objectives.model.Objective;
import fr.ca.cats.ecoboost.domain.shared.Euro;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static fr.ca.cats.ecoboost.domain.objectives.model.Periodicity.MONTHLY;
import static fr.ca.cats.ecoboost.features.shared.ObjectiveParameter.objective;
import static org.assertj.core.api.Assertions.assertThat;

class ObjectiveParameterTest {

    @Test
    void should_parse() {
        Objective objective = objective("5000.00€ at a rate of 400.00€ per month");
        assertThat(objective.globalAmount()).isEqualTo(euro("5000.00"));
        assertThat(objective.installment().amount()).isEqualTo(euro("400.00"));
        assertThat(objective.installment().periodicity()).isEqualTo(MONTHLY);
    }

    private static Euro euro(String val) {
        return Euro.of(new BigDecimal(val));
    }
}