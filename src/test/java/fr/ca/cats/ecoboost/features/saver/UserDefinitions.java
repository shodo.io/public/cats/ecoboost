package fr.ca.cats.ecoboost.features.saver;

import fr.ca.cats.ecoboost.domain.saving.model.EssentialCharges;
import fr.ca.cats.ecoboost.domain.saving.model.SalaryNet;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import util.TestContext;

import static org.assertj.core.api.Assertions.assertThat;
import static util.Users.standardProfile;

public class UserDefinitions {
    private final TestContext testContext;

    public UserDefinitions(TestContext testContext) {
        this.testContext = testContext;
    }

    @Given("a standard user")
    public void aStandardUser() {
        testContext.setCurrentProfile(standardProfile());
    }

    @Then("He/She is known as {string}")
    public void heShouldBeNamed(String pseudo) {
        assertThat(testContext.currentProfile().pseudo()).isEqualTo(pseudo);
    }

    @Then("He/She earns {salary}")
    public void earn(SalaryNet salaryNet) {
        assertThat(testContext.currentProfile().salaryNet()).isEqualTo(salaryNet);
    }

    @Then("He/She is {int} years old")
    public void age(int age) {
        assertThat(testContext.currentProfile().age()).isEqualTo(age);
    }

    @Then("His essential charges are {charges}")
    public void essentialCharges(EssentialCharges charges) {
        assertThat(testContext.currentProfile().charges()).isEqualTo(charges);
    }
}
