package fr.ca.cats.ecoboost.features.shared;

import fr.ca.cats.ecoboost.domain.shared.Percentage;
import io.cucumber.java.ParameterType;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static fr.ca.cats.ecoboost.features.shared.ObjectiveParameter.DECIMAL_REGEX;

public class PercentParameter {

    public static final String PERCCENTAGE_REGEX = DECIMAL_REGEX + "%";

    @ParameterType(PERCCENTAGE_REGEX)
    public static Percentage percentage(String percentage) {
        Pattern pattern = Pattern.compile("(" + PERCCENTAGE_REGEX + ")");
        Matcher matcher = pattern.matcher(percentage);
        if (matcher.find()) {
            return new Percentage(new BigDecimal(matcher.group(1).replace("%","")));
        } else {
            throw new IllegalArgumentException("unparsable percentage " + percentage);
        }
    }
}
