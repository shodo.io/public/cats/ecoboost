package fr.ca.cats.ecoboost.features.saver;

import fr.ca.cats.ecoboost.domain.objectives.model.Objective;
import fr.ca.cats.ecoboost.domain.saving.model.Saver;
import fr.ca.cats.ecoboost.domain.usecase.saving.DefineObjective;
import fr.ca.cats.ecoboost.domain.usecase.saving.RetrieveSaver;
import fr.ca.cats.ecoboost.infra.saver.InMemoryAccountRepository;
import fr.ca.cats.ecoboost.infra.saver.InMemorySaverRepository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import util.TestContext;

import static fr.ca.cats.ecoboost.features.shared.ObjectiveParameter.objective;
import static org.assertj.core.api.Assertions.assertThat;
import static util.Users.standardProfile;

public class SaverDefinition {
    private final DefineObjective defineObjective;
    private final RetrieveSaver retrieveSaver;
    private final TestContext testContext;

    public SaverDefinition(InMemorySaverRepository saverRepository, InMemoryAccountRepository accountRepository, TestContext testContext) {
        this.defineObjective = new DefineObjective(saverRepository, accountRepository);
        this.retrieveSaver = new RetrieveSaver(saverRepository);
        this.testContext = testContext;
    }

    @Given("a standard saver")
    public void aStandardSaver() {
        Saver saver = defineObjective.act(standardProfile(), objective("5000.00€ at a rate of 400.00€ per month")).get();
        testContext.setCurrentSaver(saver);
    }

    @Then("He/She wanted to save {objective}")
    public void heWantedToSave(Objective objective) {
        heShouldBeRegisteredAsSaver(objective);
    }

    @Then("he should be registered as saver with {objective} as objective")
    public void heShouldBeRegisteredAsSaver(Objective objective) {
        Saver saver = retrieveSaver.act(testContext.currentSaver());
        assertThat(saver.objective()).isEqualTo(objective);
    }
}
