package fr.ca.cats.ecoboost.features.shared;

import io.cucumber.java.ParameterType;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class DateParameter {

    @ParameterType(".*")
    public static LocalDateTime date(String date) {
        return LocalDate.parse(date).atTime(LocalTime.NOON);
    }
}
