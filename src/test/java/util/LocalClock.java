package util;

import fr.ca.cats.ecoboost.domain.shared.Clock;

import java.time.LocalDateTime;

public class LocalClock implements Clock {

    private LocalDateTime now;

    @Override
    public LocalDateTime now() {
        return now;
    }

    public void setNow(LocalDateTime now) {
        this.now = now;
    }
}
