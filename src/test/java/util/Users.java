package util;

import fr.ca.cats.ecoboost.domain.objectives.model.Periodicity;
import fr.ca.cats.ecoboost.domain.saving.model.EssentialCharges;
import fr.ca.cats.ecoboost.domain.saving.model.Profile;
import fr.ca.cats.ecoboost.domain.saving.model.SalaryNet;
import fr.ca.cats.ecoboost.domain.shared.Euro;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Users {

    public static Profile standardProfile() {
        return new Profile(
                "Jimmy",
                LocalDate.now().minusYears(27),
                new SalaryNet(Euro.of(BigDecimal.valueOf(2000.00)), Periodicity.MONTHLY),
                new EssentialCharges(Euro.of(BigDecimal.valueOf(1000)), Periodicity.MONTHLY));
    }
}
