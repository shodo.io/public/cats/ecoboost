package util;

import fr.ca.cats.ecoboost.domain.shared.Euro;

import java.math.BigDecimal;

public class Dsl {
    public static Euro euro(Integer value){
        return Euro.of(BigDecimal.valueOf(value));
    }
}
