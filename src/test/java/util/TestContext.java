package util;

import fr.ca.cats.ecoboost.domain.configuration.ObjectiveCheckConfiguration;
import fr.ca.cats.ecoboost.domain.saving.model.Profile;
import fr.ca.cats.ecoboost.domain.saving.model.Saver;
import fr.ca.cats.ecoboost.domain.saving.model.SaverId;
import fr.ca.cats.ecoboost.domain.usecase.saving.UnReachableObjectiveError;

public class TestContext {

    private Profile currentProfile;
    private SaverId currentSaver;
    private ObjectiveCheckConfiguration objectiveCheckConfiguration;

    public void setCurrentSaver(Saver currentSaver) {this.currentSaver = currentSaver.id();this.currentProfile = currentSaver.profile();}
    public void setCurrentProfile(Profile currentProfile) {this.currentProfile = currentProfile;}
    public Profile currentProfile() {return currentProfile;}
    public SaverId currentSaver() {return currentSaver;}

    private UnReachableObjectiveError currentError;
    public UnReachableObjectiveError currentError() {return currentError;}
    public void setCurrentError(UnReachableObjectiveError currentError) {this.currentError = currentError;}

    public void setObjectiveConfiguration(ObjectiveCheckConfiguration objectiveCheckConfiguration) {
        this.objectiveCheckConfiguration = objectiveCheckConfiguration;
    }

    public ObjectiveCheckConfiguration objectiveCheckConfiguration() {
        return objectiveCheckConfiguration;
    }
}
